Feature: Test falling words app

  # Assumptions:
  # Game session length depends on number of words in words.json file
  # Score should be 0 after the each new session

  Background:
    Given User is on start screen
    And User starts game

  Scenario: Test changing words
    When Get current english word
    And User doesn't take any actions for "10" seconds
    Then Words got changed
    And Score is "0"

  Scenario: Test only correct answers
    When Provide answer for "297" times
    Then Result should be equal "297"
    And Start button and title are displayed
    And User starts game
    And Score is "0"

  Scenario Outline: Test answer combinations
    When Wait for the "<translation>" translation
    And Score is "<actual_score>"
    And Tap on "<tap_button>" button
    Then Score is "<expected_score>"
    Examples:
      | translation | actual_score | tap_button       | expected_score |
      | CORRECT     | 0            | CORRECT_ANSWER   | 1              |
      | INCORRECT   | 1            | INCORRECT_ANSWER | 2              |
      | INCORRECT   | 2            | CORRECT_ANSWER   | 1              |
      | CORRECT     | 1            | INCORRECT_ANSWER | 0              |


    # Addtional tests to be implemented:
  # 1. Test navigation back
  # 2. Test negative value for score
