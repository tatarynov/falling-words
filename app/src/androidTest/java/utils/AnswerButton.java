package utils;

public enum AnswerButton {
    CORRECT_ANSWER,
    INCORRECT_ANSWER
}
