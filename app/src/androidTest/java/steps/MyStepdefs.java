package steps;

import android.app.Activity;
import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.babbel.fallingwords.MainActivity;
import com.babbel.fallingwords.R;
import com.babbel.fallingwords.data.WordPair;
import com.babbel.fallingwords.data.WordRepositoryImpl;

import org.junit.Rule;

import java.util.List;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertNotNull;
import static utils.AnswerButton.CORRECT_ANSWER;
import static utils.AnswerButton.INCORRECT_ANSWER;
import static utils.Translation.CORRECT;
import static utils.Translation.INCORRECT;


public class MyStepdefs {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    private Activity activity;
    private List<WordPair> words;

    public static final int WAIT_TIMEOUT = 60;
    public static final int POLLING_TIMEOUT_MS = 500;

    private static String wordIntermitentStorage;

    @Before
    public void setup() {
        activityTestRule.launchActivity(new Intent());
        activity = activityTestRule.getActivity();
        words = new WordRepositoryImpl(activity.getAssets()).getWords();
    }

    @After
    public void tearDown() {
        activityTestRule.finishActivity();
    }

    @Given("^User is on start screen$")
    public void user_is_on_start_screen() {
        assertNotNull(activity);
    }

    @Given("^User starts game$")
    public void userStartsGame() {
        onView(withId(R.id.start_button)).perform(click());
    }

    @When("^Wait for the \"([^\"]*)\" translation$")
    public void waitForTheTranslation(String translationType) throws InterruptedException {
        translationType = translationType.toUpperCase();
        for (int i = 0; i < WAIT_TIMEOUT; i++) {
            boolean match = isCorrectTranslation();
            if (match && translationType.equals(CORRECT.toString())) {
                return;
            } else if (!match && translationType.equals(INCORRECT.toString())) {
                return;
            }

            Thread.sleep(1000);
        }

        throw new IllegalStateException(String.format("No words with %s translation appeared!", translationType));
    }


    @And("^Score is \"([^\"]*)\"$")
    public void scoreIs(String scoreValue) {
        onView(withId(R.id.text_score)).check(matches(withText(scoreValue)));
    }

    @And("^Tap on \"([^\"]*)\" button$")
    public void tapOnButton(String arg0) throws InterruptedException {
        Log.d("[DEBUG]", String.format("Clicking ong %s button", arg0.toUpperCase()));
        Log.d("[DEBUG]", String.format("Enum value %s", CORRECT_ANSWER.toString()));
        int buttonId = arg0.toUpperCase().equals(CORRECT_ANSWER.toString()) ?
                R.id.image_correct : R.id.image_incorrect;
        onView(withId(buttonId)).perform(click());
        Thread.sleep(1000);
    }

    @When("^User doesn't take any actions for \"([^\"]*)\" seconds$")
    public void userDoesnTTakeAnyActionsForSeconds(String arg0) throws Throwable {
        waitFor(Integer.valueOf(arg0) * 1000);
    }

    @Then("^Words got changed$")
    public void wordsGotChanged() {
        onView(withText(wordIntermitentStorage)).check(matches(not(isDisplayed())));
    }

    @When("^Get current english word$")
    public void getCurrentEnglishWord() {
        wordIntermitentStorage = getTextViewValue(activity.findViewById(R.id.text_first_word));
        Log.d("[DEBUG]", "SAVED WORD: <" + wordIntermitentStorage + ">");
    }

    @When("^Provide answer for \"([^\"]*)\" times$")
    public void provideAnswerForTimes(int times) throws Throwable {
        for (int i = 0; i < times; i++) {
            if (isCorrectTranslation()) {
                tapOnButton(CORRECT_ANSWER.toString());
            } else {
                tapOnButton(INCORRECT_ANSWER.toString());
            }
        }
    }

    @Then("^Result should be equal \"([^\"]*)\"$")
    public void resultShouldBeEqual(String arg0) {
        onView(withId(R.id.score_count)).check(matches(withText(arg0)));
    }

    @And("^Start button and title are displayed$")
    public void startButtonIsDisplayed() {
        onView(withText(R.string.app_name)).check(matches(isDisplayed()));
        onView(withId(R.id.start_button)).check(matches(isDisplayed()));
    }

    private String getTranslation(String wordInEng) {
        for (WordPair word : words) {
            if (word.wordInEnglish().toLowerCase().equals(wordInEng.toLowerCase())) {
                return word.wordInSpanish();
            }
        }

        throw new IllegalArgumentException(String.format("No translation for <%s> word", wordInEng));
    }

    private String getTextViewValue(View view) {
        return ((TextView) view).getText().toString();
    }

    private void waitFor(long timeoutInMs) throws InterruptedException {
        long start = System.currentTimeMillis();
        while ((System.currentTimeMillis() - start) < timeoutInMs) {
            Thread.sleep(POLLING_TIMEOUT_MS);
        }
    }

    private boolean isCorrectTranslation() {
        String wordEn = getTextViewValue(activity.findViewById(R.id.text_first_word));
        String correctTranslation = getTranslation(wordEn);
        String wordEs = getTextViewValue(activity.findViewById(R.id.text_second_word));

        Log.d("[DEBUG]", "Text of english word element: <" + wordEn + ">");
        Log.d("[DEBUG]", " Correct translation: <" + correctTranslation + ">");
        Log.d("[DEBUG]", "Text of spanish word element: <" + wordEs + ">");

        return wordEs.equals(correctTranslation);
    }
}