package com.babbel.fallingwords;

import android.app.Application;
import com.babbel.fallingwords.data.WordPair;
import com.babbel.fallingwords.data.WordRepositoryImpl;
import com.babbel.fallingwords.data.WordsRepositoryProvider;

import java.util.ArrayList;
import java.util.Collections;

public class FallingWordsApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        WordsRepositoryProvider.getInstance()
                .setWordsRepository(new WordRepositoryImpl(getAssets()));
    }
}
