package com.babbel.fallingwords.data;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

@AutoValue
public abstract class WordPair {

    public static TypeAdapter<WordPair> typeAdapter(Gson gson) {
        return new AutoValue_WordPair.GsonTypeAdapter(gson);
    }

    public static Builder builder() {
        return new AutoValue_WordPair.Builder();
    }

    @SerializedName("text_eng")
    public abstract String wordInEnglish();

    @SerializedName("text_spa")
    public abstract String wordInSpanish();

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder wordInEnglish(String wordInEnglish);

        public abstract Builder wordInSpanish(String wordInSpanish);

        public abstract WordPair build();
    }
}
