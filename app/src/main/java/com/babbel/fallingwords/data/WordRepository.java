package com.babbel.fallingwords.data;

import java.util.List;

public interface WordRepository {
    List<WordPair> getWords();
}
