package com.babbel.fallingwords.data;

import android.content.res.AssetManager;
import android.support.annotation.NonNull;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WordRepositoryImpl implements WordRepository {
    private static final Gson GSON = new GsonBuilder()
            .registerTypeAdapterFactory(WordTypeAdapterFactory.create())
            .create();

    @NonNull
    private final AssetManager assetManager;
    @NonNull
    private List<WordPair> words = Collections.emptyList();

    public WordRepositoryImpl(@NonNull AssetManager assetManager) {
        this.assetManager = assetManager;
    }

    @Override
    public List<WordPair> getWords() {
        try {
            words = GSON.fromJson(
                    new InputStreamReader(assetManager.open("words.json")),
                    new TypeToken<ArrayList<WordPair>>() {
                    }.getType());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return words;
    }
}
