package com.babbel.fallingwords.data;

import android.support.annotation.NonNull;

public class WordsRepositoryProvider {
    private WordRepository wordRepository;

    private static class LazyHolder {
        static final WordsRepositoryProvider INSTANCE = new WordsRepositoryProvider();
    }

    public static WordsRepositoryProvider getInstance() {
        return LazyHolder.INSTANCE;
    }

    private WordsRepositoryProvider() {
    }

    public WordRepository getWordsRepository() {
        return wordRepository;
    }

    public void setWordsRepository(@NonNull WordRepository wordRepository) {
        this.wordRepository = wordRepository;
    }
}
