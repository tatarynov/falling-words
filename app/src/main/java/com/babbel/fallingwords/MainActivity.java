package com.babbel.fallingwords;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ViewFlipper;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.babbel.fallingwords.screens.GameScreen;
import com.babbel.fallingwords.screens.StartScreen;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.main_container)
    ViewFlipper flipper;
    @BindView(R.id.game_screen)
    GameScreen gameScreen;
    @BindView(R.id.start_screen)
    StartScreen startScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
    }

    private void showStartScreen() {
        flipper.setDisplayedChild(0);
    }

    private void showGameScreen() {
        flipper.setDisplayedChild(1);
    }

    @OnClick(R.id.start_button)
    public void onStartButtonClicked() {
        gameScreen.setOnGameSessionFinishedListener(score -> {
            showStartScreen();
            startScreen.onGameFinished(score);
        });

        startScreen.onGameStarted();
        showGameScreen();
        gameScreen.startGameSession();
    }

    @Override
    public void onBackPressed() {
        if (flipper.getDisplayedChild() == 1) {
            showStartScreen();
            return;
        }
        super.onBackPressed();
    }
}
