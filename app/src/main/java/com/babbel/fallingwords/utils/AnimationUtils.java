package com.babbel.fallingwords.utils;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.widget.TextView;

public class AnimationUtils {

    public static Animator slide(final TextView textView, float from, float to) {
        return ObjectAnimator.ofFloat(textView, "y", from, to)
                .setDuration(5000);
    }
}
