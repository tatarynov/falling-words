package com.babbel.fallingwords.utils;

import android.content.Context;
import android.media.AudioManager;
import android.os.Vibrator;

import static android.content.Context.VIBRATOR_SERVICE;

public class VibrationUtils {

    public static void runShortVibration(Context context) {
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        switch (am.getRingerMode()) {
            case AudioManager.RINGER_MODE_VIBRATE:
            case AudioManager.RINGER_MODE_NORMAL:
                ((Vibrator) context.getSystemService(VIBRATOR_SERVICE)).vibrate(50);
                break;
        }
    }

    public static void runLongVibration(Context context) {
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        switch (am.getRingerMode()) {
            case AudioManager.RINGER_MODE_VIBRATE:
            case AudioManager.RINGER_MODE_NORMAL:
                ((Vibrator) context.getSystemService(VIBRATOR_SERVICE)).vibrate(500);
                break;
        }
    }
}
