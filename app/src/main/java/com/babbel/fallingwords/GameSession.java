package com.babbel.fallingwords;

import android.support.annotation.Nullable;
import com.babbel.fallingwords.data.WordPair;
import com.babbel.fallingwords.data.WordRepository;
import com.babbel.fallingwords.data.WordsRepositoryProvider;

import java.util.List;
import java.util.Random;

public class GameSession {
    private List<WordPair> wordPairs;
    private WordPair actualWordPair;
    private String testWord;
    private Random random = new Random();
    private static int score;
    @Nullable
    private OnGameSessionFinishedListener onGameSessionFinishedListener;

    public GameSession() {
    }

    public void init() {
        WordRepository repository = WordsRepositoryProvider.getInstance().getWordsRepository();
        wordPairs = repository.getWords();
        next();
    }

    public void next() {
        if (wordPairs.isEmpty()) {
            if (onGameSessionFinishedListener != null) {
                onGameSessionFinishedListener.onGameSessionFinished(score);
            }
            return;
        }

        int index = getRandomNumber(wordPairs.size());
        actualWordPair = wordPairs.remove(index);
        testWord = getTestWord();
    }

    private int getRandomNumber(int max) {
        return random.nextInt(max);
    }

    private boolean getRandomBoolean() {
        return random.nextBoolean();
    }

    public String actualEnglishWord() {
        return actualWordPair.wordInEnglish();
    }

    public String actualSpanishWord() {
        return testWord;
    }

    private String getTestWord() {
        boolean randomBoolean = getRandomBoolean();
        if (randomBoolean || wordPairs.isEmpty()) {
            // Return correct spanish word solution
            return actualWordPair.wordInSpanish();
        } else {
            // Return random spanish word
            // There is a small chance that the random word is the same as the correct word
            return wordPairs.get(getRandomNumber(wordPairs.size())).wordInSpanish();
        }
    }

    private boolean isTranslationCorrect() {
        return (testWord.equals(actualWordPair.wordInSpanish()));
    }

    // User answer: parameter is true when user taps "correct button"
    public void answer(boolean correct) {
        boolean result = isTranslationCorrect() == correct;
        if (result) {
            score++;
        } else {
            score--;
        }
    }

    public void noAnswerInTime() {

    }

    public int getScore() {
        return score;
    }

    public void setOnGameSessionFinishedListener(@Nullable OnGameSessionFinishedListener onGameSessionFinishedListener) {
        this.onGameSessionFinishedListener = onGameSessionFinishedListener;
    }

    @Override
    public String toString() {
        return "GameSession{" +
                "actualWordPair=" + actualWordPair +
                ", testWord='" + testWord + '\'' +
                ", score=" + score +
                '}';
    }

    public interface OnGameSessionFinishedListener {
        /**
         * Called when there are no more words to display
         */
        void onGameSessionFinished(int score);
    }
}
