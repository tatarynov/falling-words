package com.babbel.fallingwords.screens;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.babbel.fallingwords.R;

public class StartScreen extends ConstraintLayout {
    @BindView(R.id.score_table)
    View scoreTable;
    @BindView(R.id.score_count)
    TextView scoreCount;
    @BindView(R.id.greeting)
    TextView greeting;

    public StartScreen(Context context) {
        super(context);
    }

    public StartScreen(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public StartScreen(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private static final String GREETINGS[] = new String[]{
            "Great!", "Awesome!", "Nice Job!", "Let's go again?", "One more time?"
    };

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        ButterKnife.bind(this);
    }

    public void onGameFinished(int score) {
        scoreCount.setText(String.valueOf(score));
        greeting.setText(GREETINGS[score % GREETINGS.length]);

        scoreTable.setVisibility(View.VISIBLE);
    }

    public void onGameStarted() {
        scoreTable.setVisibility(View.INVISIBLE);
    }
}
