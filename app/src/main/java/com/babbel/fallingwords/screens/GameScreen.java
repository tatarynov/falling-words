package com.babbel.fallingwords.screens;

import android.animation.Animator;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.babbel.fallingwords.GameSession;
import com.babbel.fallingwords.R;
import com.babbel.fallingwords.utils.AnimationUtils;
import com.babbel.fallingwords.utils.VibrationUtils;

public class GameScreen extends ConstraintLayout {
    public GameScreen(Context context) {
        super(context);
    }

    public GameScreen(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public GameScreen(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @BindView(R.id.image_correct)
    ImageView correct;
    @BindView(R.id.image_incorrect)
    ImageView incorrect;
    @BindView(R.id.text_first_word)
    TextView firstWord;
    @BindView(R.id.text_second_word)
    TextView secondWord;
    @BindView(R.id.text_score)
    TextView scoreCounter;

    private GameSession gameSession;

    @Nullable
    private GameSession.OnGameSessionFinishedListener onGameSessionFinishedListener;

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    public void setOnGameSessionFinishedListener(@Nullable GameSession.OnGameSessionFinishedListener onGameSessionFinishedListener) {
        this.onGameSessionFinishedListener = onGameSessionFinishedListener;
    }

    public void startGameSession() {
        gameSession = new GameSession();
        gameSession.init();
        gameSession.setOnGameSessionFinishedListener(onGameSessionFinishedListener);
        updateUi();
        animateSecondWord();
    }

    private void animateSecondWord() {
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                getViewTreeObserver().removeOnGlobalLayoutListener(this);
                Animator animator = AnimationUtils.slide(
                        secondWord,
                        firstWord.getY() + firstWord.getHeight(),
                        secondWord.getRootView().getHeight());
                animator.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        secondWord.clearAnimation();
                        gameSession.noAnswerInTime();
                        VibrationUtils.runLongVibration(getContext());
                        next();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
                animator.start();
            }
        });
    }

    @OnClick(R.id.image_correct)
    public void onClickImageCorrect() {
        gameSession.answer(true);
        VibrationUtils.runShortVibration(getContext());
        next();
    }

    @OnClick(R.id.image_incorrect)
    public void onClickImageIncorrect() {
        gameSession.answer(false);
        VibrationUtils.runShortVibration(getContext());
        next();
    }

    private void updateUi() {
        firstWord.setText(gameSession.actualEnglishWord());
        secondWord.setText(gameSession.actualSpanishWord());

        scoreCounter.setText(String.valueOf(gameSession.getScore()));
    }

    private void next() {
        gameSession.next();
        updateUi();
        animateSecondWord();
    }
}
