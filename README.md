# Android Falling Words 

## Description

Falling Words is an Android App Game for learning Spanish. 

When the game starts an English word appears on top and a Spanish word below it.
The Spanish word will fall to the bottom of the screen in 5 seconds. 
Within this time, the you can answer if the Spanish word is the correct translation.

## Game Start

When you hit the start button the game will start with a score of 0. Words will start falling from the
top, so you'll need to start answering.

The game finishes when the words finish. At the end of the game, you'll be back to the start screen
and your score will be shown there.

This score is not persisted, meaning if you restart the app it'll reset itself. The same is true if you
start a new game.

## Scoring

At the bottom of the screen there is a score counter. This will increment when the answer is correct,
decrement when it's incorrect and not change when no answer is given. The increments and decrements are
always an amount of 1.

If you mark a translation as wrong, i.e. you say 'ajedrez' (Chess) is not the correct translation for
'sunshine', you'll still score a point. A similar logic applies if you say the translation is wrong, but
was indeed correct - your score will be decremented.


## Run tests

To run tests use next command:
```
gradle connectedCheck
```
